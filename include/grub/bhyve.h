/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2020  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRUB_BHYVE_H
#define GRUB_BHYVE_H 1

#ifndef __FreeBSD__
#error bhyve only applies for FreeBSD systems
#endif

#include <grub/types.h>
#include <grub/i386/relocator.h>

#define BIT(b)              (1 << (b))
/* Disk image is a CD-ROM */
#define GRUB_DISKIMG_F_CD   BIT(0)
/* Disk image has partitions */
#define GRUB_DISKIMG_F_PART BIT(1)

#define	DEFAULT_GUESTMEM	256

struct grub_bhyve_info {
  int bootsz;
  int nsegs;
  struct grub_mmap_region *segs;
};

struct bhyvedisk_data {
  int fd;
  grub_off_t diskimg_size;
  grub_uint32_t diskimg_sect;
  grub_uint32_t flags;
};

int grub_bhyve_init (const char *name, grub_uint64_t memsz);
void EXPORT_FUNC (grub_bhyve_boot32) (grub_uint32_t bt, struct grub_relocator32_state rs);
void EXPORT_FUNC (grub_bhyve_boot64) (struct grub_relocator64_state rs);
const struct grub_bhyve_info * EXPORT_FUNC(grub_bhyve_get_info) (void);
void *EXPORT_FUNC (grub_bhyve_virt) (grub_uint64_t physaddr);
int grub_bhyve_parse_memsize (const char *arg, grub_uint64_t *size);
void grub_bhyve_unset_cinsert (void);
int EXPORT_FUNC (grub_bhyve_cinsert) (void);
void grub_bhyve_unset_vgainsert (void);
int EXPORT_FUNC (grub_bhyve_vgainsert) (void);
int grub_bhyve_memwire_avail (void);
void grub_bhyve_set_memwire (void);
void grub_bhyve_diskimg_init (void);
void grub_bhyve_diskimg_fini (void);
int grub_diskimg_add (const char * diname);
struct bhyvedisk_data *grub_diskimg_get_device (int num);

#endif /* GRUB_BHYVE_H */
