/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2020  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <config-util.h>

#include <grub/disk.h>
#include <grub/dl.h>
#include <grub/kernel.h>
#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/types.h>
#include <grub/bhyve.h>

#include <grub/emu/hostdisk.h>
#include <grub/emu/misc.h>

#include <stdio.h>

GRUB_MOD_LICENSE ("GPLv3+");

struct bhyvedisk_data *hd_devices;
static int hd_num;
static int hd_max;

int
grub_diskimg_add(const char * diname)
{
  int fd;
  struct bhyvedisk_data *d;

  fd = grub_util_fd_open (diname, GRUB_UTIL_FD_O_RDONLY);
  if (!GRUB_UTIL_FD_IS_VALID (fd))
    {
      grub_printf ("cannot open `%s': %s", diname,
          grub_util_fd_strerror ());
      return GRUB_ERR_FILE_NOT_FOUND;
    }

  if (hd_num == hd_max)
    {
      int new_num;
      new_num = (hd_max ? hd_max * 2 : 1);
      d = grub_realloc(hd_devices,
           sizeof (struct bhyvedisk_data) * new_num);
      if (!d)
        return grub_errno;
      hd_devices = d;
      hd_max = new_num;
    }

  d = &hd_devices[hd_num];
  hd_num++;

  d->fd = fd;
  d->diskimg_size = grub_util_get_fd_size (fd, diname, &d->diskimg_sect);
  d->flags = 0;
  if (grub_strstr (diname, ".iso") != NULL)
    d->flags |= GRUB_DISKIMG_F_CD;

  /*TODO assume device is partitioned */
  if ((d->flags & GRUB_DISKIMG_F_CD) == 0)
    d->flags |= GRUB_DISKIMG_F_PART;

  return GRUB_ERR_NONE;
}

static int
grub_diskimg_iterate (grub_disk_dev_iterate_hook_t hook, void *hook_data,
		      grub_disk_pull_t pull)
{
  char buf[16];
  int count;
  int nchr;

  if (pull != GRUB_DISK_PULL_NONE)
    return 0;

  /* "hd" - built-in mass-storage */
  for (count = 0 ; count < hd_num; count++)
    {
      nchr = grub_snprintf (buf, sizeof (buf) - 1, "%cd%u",
          hd_devices[count].flags & GRUB_DISKIMG_F_CD ? 'c' : 'h',
          count);
      if (hd_devices[count].flags & GRUB_DISKIMG_F_PART)
        grub_snprintf (buf + nchr, sizeof (buf) - nchr, ",1");

      if (hook (buf, hook_data))
        return 1;
    }

  return 0;
}

/* Helper function for grub_diskimg_open. */
struct bhyvedisk_data *
grub_diskimg_get_device (int num)
{
  if (num < hd_num)
    return &hd_devices[num];

  return NULL;
}

static grub_err_t
grub_diskimg_open (const char *name, grub_disk_t disk)
{
  struct bhyvedisk_data *d;
  int num;

  num = grub_strtoul (name + 2, 0, 10);
  if (grub_errno != GRUB_ERR_NONE)
    {
      /*fprintf (stderr, "%s: Opening '%s' failed, invalid number\n",
		    __func__, name);*/
      goto fail;
    }

  if (name[1] != 'd')
    {
      fprintf (stderr, "%s: Opening '%s' failed, invalid name\n", __func__,
          name);
      goto fail;
    }

  switch (name[0])
    {
    case 'c':
    case 'h':
      d = grub_diskimg_get_device (num);
      break;
    default:
      goto fail;
    }

  if (!d)
    goto fail;

  if (d->diskimg_sect == 0)
    d->diskimg_sect = GRUB_DISK_SECTOR_SIZE;

  disk->total_sectors = d->diskimg_size >> disk->log_sector_size;
  disk->max_agglomerate = GRUB_DISK_MAX_MAX_AGGLOMERATE;
  disk->id = d->fd;
  disk->data = d;

  return GRUB_ERR_NONE;

fail:
  return grub_error (GRUB_ERR_UNKNOWN_DEVICE, "no such device");
}

static void
grub_diskimg_close (grub_disk_t disk __attribute((unused)))
{
/*XXX grub_printf("%s(%p)\n", __func__, disk);*/
}

static grub_err_t
grub_diskimg_read (grub_disk_t disk __attribute((unused)), grub_disk_addr_t sector,
		    grub_size_t size, char *buf)
{
  struct bhyvedisk_data *d;
  ssize_t bytes;
  uint32_t ashift;

  d = disk->data;
  if (!d)
    return grub_error (GRUB_ERR_BAD_DEVICE, "no device data");

  ashift = disk->log_sector_size;
  bytes = pread(d->fd, buf, size << ashift, sector << ashift);
  if (bytes != (size << ashift))
    {
      if (bytes == -1)
        return grub_error (GRUB_ERR_READ_ERROR, "read error");
      else
        return GRUB_ERR_READ_ERROR;
    }

  return GRUB_ERR_NONE;
}

static grub_err_t
grub_diskimg_write (grub_disk_t disk __attribute((unused)), grub_disk_addr_t sector,
		     grub_size_t size, const char *buf)
{
/*XXX*/grub_printf("%s(disk=%p, sector=%ld, size=%ld, buf=%p)\n", __func__,
    disk, sector, size, buf);
  return 0;
}

static struct grub_disk_dev grub_diskimg_dev =
  {
    .name = "diskimg",
    .id = GRUB_DISK_DEVICE_DISKIMG_ID,
    .disk_iterate = grub_diskimg_iterate,
    .disk_open = grub_diskimg_open,
    .disk_close = grub_diskimg_close,
    .disk_read = grub_diskimg_read,
    .disk_write = grub_diskimg_write,
    .next = 0
  };

void
grub_bhyve_diskimg_init(void)
{
  hd_num = 0;
  hd_max = 0;

  grub_disk_dev_register (&grub_diskimg_dev);
}

void
grub_bhyve_diskimg_fini(void)
{
  if (hd_devices)
    grub_free(hd_devices);

  grub_disk_dev_unregister (&grub_diskimg_dev);
}

/* vim: se tw=79 ts=2 sw=2 et cinoptions={1s: */
