/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2020  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <config-util.h>

#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#include <grub/bhyve.h>
#include <grub/emu/console.h>
#include <grub/emu/hostdisk.h>
#include <grub/env.h>
#include <grub/err.h>
#include <grub/kernel.h>
#include <grub/loader.h>
#include <grub/memory.h>
#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/normal.h>
#include <grub/script_sh.h>
#include <grub/setjmp.h>
#include <grub/search.h>
#include <grub/syslinux_parse.h>
#include <grub/term.h>
#include <grub/util/misc.h>

#include "progname.h"
#include <argp.h>

#define MB (1024 *1024)
#define ROOT_NAME "bhyve_root"
/* Maximum number of disks (arbitrary) */
#define MAX_DNAMES  4

/* Used for going back to the main function.  */
static jmp_buf main_env;

grub_addr_t grub_modbase = 0;

static const char *grub_paths[] = {
  "/grub2",  /* e.g. CentOS with separate boot partition */
  "/boot/grub", /* e.g. Ubuntu with single partition */
  NULL
};

static const char *syslinux_paths[] = {
  "/isolinux/isolinux.cfg",
  "/extlinux.conf",
  NULL
};

static char *grub_path_user = NULL;
static char *grub_root_user = NULL;

void
grub_reboot (void)
{
  longjmp (main_env, 1);
  grub_fatal ("longjmp failed");
}

void
grub_machine_get_bootlocation (char **device, char **path)
{
#define NAMESZ 64
  char buf[NAMESZ];
  const char **gpath;
  const char *root;
  struct bhyvedisk_data *d;
  int nchr;

  /* In compatibility mode, do what we're told and no more */
  if (grub_path_user || grub_root_user)
    {
      if (grub_path_user)
        *path = grub_strdup (grub_path_user);
      if (grub_root_user)
        *device = grub_strdup (grub_root_user);
      return;
    }

  /* If the boot device has grub.cfg, set the path */
  gpath = grub_paths;
  while (*gpath != NULL)
    {
      grub_snprintf(buf, sizeof (buf), "%s/grub.cfg", *gpath);
      grub_search_fs_file (buf, ROOT_NAME, 0, NULL, 0);
      root = grub_env_get (ROOT_NAME);
      if (root != NULL)
        {
          *device = grub_strdup (root);
          *path = grub_strdup (*gpath);
          grub_env_unset (ROOT_NAME);

          return;
        }
      gpath++;
    }

  /* Check if the boot device uses syslinux */
  gpath = syslinux_paths;
  while (*gpath != NULL)
    {
      grub_search_fs_file (*gpath, ROOT_NAME, 0, NULL, 0);
      root = grub_env_get (ROOT_NAME);
      if (root != NULL)
        {
          char *cfg;

          *device = grub_strdup (root);

          cfg = grub_malloc (1024);
          if (cfg != NULL)
            {
              grub_snprintf (cfg, 1024, "syslinux_configfile %s", *gpath);
              grub_load_config_dynamic(cfg, grub_strlen (cfg));
              grub_free (cfg);
            }

          grub_env_unset (ROOT_NAME);

          return;
        }
      gpath++;
    }

  /* Failing all else, set the first device as the root */
  d = grub_diskimg_get_device(0);
  if (d == NULL)
    return;

  nchr = grub_snprintf(buf, NAMESZ, "%cd0",
      d->flags & GRUB_DISKIMG_F_CD ? 'c' : 'h');
  if (d->flags & GRUB_DISKIMG_F_PART)
    {
      grub_snprintf(buf + nchr, NAMESZ - nchr, ",1");
    }
  *device = grub_strdup (buf);
}

void *
grub_memalign (grub_size_t align, grub_size_t size)
{
  return (grub_malloc (size));
}

static struct argp_option options[] = {
  {"cons-dev", 'c', N_("cons-dev"), 0,
    N_("a tty(4) device to use for terminal I/O"), 0},
  {"root",      'r', N_("DEVICE_NAME"), 0, N_("Set root device."), 2},
  {"device-map",  'm', N_("FILE"), 0,
   /* TRANSLATORS: There are many devices in device map.  */
   N_("use FILE as the device map [default=%s]"), 0},
  {"directory",  'd', N_("DIR"), 0,
   N_("use GRUB files in the directory DIR [default=%s]"), 0},
  {"disk",     'D', N_("disk-img"), 0,
    N_("disk image file name"), 0},
  {"evga",     'e', 0,              0,
    N_("exclude VGA rows/cols from bootinfo"), 0},
  {"hold",     'H', N_("SECS"),     OPTION_ARG_OPTIONAL,
    N_("wait until a debugger will attach"), 0},
  {"memory",   'M', N_("MBYTES"),   0,
    N_("guest RAM in MB [default=%d]"), 0},
  {"ncons",    'n', 0,              0,
    N_("disable insertion of console=ttys0"), 0},
  {"vm",       'v', N_("vm"), 0,
    N_("virtual machine name"), 0},
  {"wire",     'S', 0, 0,
    N_("Force wiring of guest memory."), 0},
  { 0, 0, 0, 0, 0, 0 }
};

#pragma GCC diagnostic ignored "-Wformat-nonliteral"

static char *
help_filter (int key, const char *text, void *input __attribute__ ((unused)))
{
  switch (key)
    {
    case 'd':
      return xasprintf (text, DEFAULT_DIRECTORY);
    case 'm':
      return xasprintf (text, DEFAULT_DEVICE_MAP);
    case 'M':
      return grub_xasprintf (text, DEFAULT_GUESTMEM);
    default:
      return (char *) text;
    }
}

#pragma GCC diagnostic error "-Wformat-nonliteral"

struct arguments
{
  grub_uint64_t memsz;
  const char *vmname;
  const char *diskname[MAX_DNAMES];
  int n_diskname;
  int hold;
};

/* Adapted from read_device_map() in kern/emu/hostdisk.c */
static void
grub_bhyve_convert_device_map (struct arguments *arguments, char *dev_map)
{
  FILE *fp;
  char buf[1024];	/* XXX */
  int lineno = 0;

  if (!dev_map || dev_map[0] == '\0')
    {
      grub_util_info ("no device.map");
      return;
    }

  fp = grub_util_fopen (dev_map, "r");
  if (! fp)
    {
      grub_util_info (_("cannot open `%s': %s"), dev_map, strerror (errno));
      return;
    }

  while (fgets (buf, sizeof (buf), fp))
    {
      char *p = buf;
      char *e;
      char *drive_e, *drive_p;
      unsigned long idx;

      lineno++;

      /* Skip leading spaces.  */
      while (*p && grub_isspace (*p))
        p++;

      /* If the first character is `#' or NUL, skip this line.  */
      if (*p == '\0' || *p == '#')
        continue;

      if (*p != '(')
        {
          char *tmp;
          tmp = xasprintf (_("missing `%c' symbol"), '(');
          grub_util_error ("%s:%d: %s", dev_map, lineno, tmp);
        }

      p++;
      /* Find a free slot.  */
      if (arguments->n_diskname == MAX_DNAMES)
        grub_util_error ("%s:%d: %s", dev_map, lineno, _("device count exceeds limit"));

      e = p;
      p = strchr (p, ')');
      if (! p)
        {
          char *tmp;
          tmp = xasprintf (_("missing `%c' symbol"), ')');
          grub_util_error ("%s:%d: %s", dev_map, lineno, tmp);
        }

      /* Support a subset of devices, specifically for bhyve */
      if ((e[0] == 'h' || e[0] == 'c') && e[1] == 'd')
        {
          char *ptr = NULL;

          idx = grub_strtoul (e + 2, (const char **)&ptr, 10);
          if (idx >= MAX_DNAMES)
            grub_util_error ("%s:%d: %s", dev_map, lineno, _("device index exceeds limit"));

          if (*ptr == ',')
            {
              *p = 0;

              /* TRANSLATORS: Only one entry is ignored. However the suggestion
                 is to correct/delete the whole file.
                 device.map is a file indicating which
                 devices are available at boot time. Fedora populated it with
                 entries like (hd0,1) /dev/sda1 which would mean that every
                 partition is a separate disk for BIOS. Such entries were
                 inactive in GRUB due to its bug which is now gone. Without
                 this additional check these entries would be harmful now.
              */
              grub_util_warn (_("the device.map entry `%s' is invalid. "
              "Ignoring it. Please correct or "
              "delete your device.map"), e);
              continue;
            }
        }
      else
        {
          grub_util_warn (_("the device.map entry `%s' is invalid. "
          "Ignoring it. Please correct or "
          "delete your device.map"), e);
          continue;
        }
      drive_e = e;
      drive_p = p;

      p++;
      /* Skip leading spaces.  */
      while (*p && grub_isspace (*p))
        p++;

      if (*p == '\0')
        grub_util_error ("%s:%d: %s", dev_map, lineno, _("filename expected"));

      /* NUL-terminate the filename.  */
      e = p;
      while (*e && ! grub_isspace (*e))
        e++;
      *e = '\0';

      {
        struct stat st;

        if (stat (p, &st) == -1)
          {
            grub_util_info ("Cannot stat `%s', skipping", p);
            continue;
          }
      }

      if (arguments->diskname[idx] != NULL)
        {
          grub_util_warn (_("drive name %s is used more than once, skipping"),
              drive_e);
          continue;
        }
      arguments->diskname[idx] = grub_strdup (p);

      if (!arguments->diskname[idx])
          grub_util_error (_("the drive name `%s' in device.map is incorrect. "
                "Please use the form [hfc]d[0-9]* "
                "(E.g. `hd0' or `cd')"),
              drive_e);

      if (idx >= arguments->n_diskname)
        arguments->n_diskname = idx + 1;

      grub_util_info ("adding `%s' from device.map",
          arguments->diskname[idx]);
    }

  fclose (fp);
}

static error_t
argp_parser (int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;

  /*
   * Some arguments are supported for compatibility with the older
   * grub-bhyve versions. These include:
   *  device-map
   *  root
   *  directory
   */
  switch (key)
    {
    case 'c':
      /* TODO grub_bhyve_set_console_dev (grub_strdup (arg));*/
      break;
    case 'm':
      /* Convert a device map file into disk-image equivalents */
      grub_bhyve_convert_device_map (arguments, arg);
      break;
    case 'r':
      /*
       * Set the root device. grub-bhyve assumes first disk-image specified
       * is the root device. Use root if this is not the case.
       */
      grub_root_user = grub_strdup (arg);
      break;
    case 'd':
      /*
       * Search directory for grub files. Not necessary for common locations,
       * but honored if set.
       */
      grub_path_user = grub_strdup (arg);
      break;
    case 'D':
      if (arguments->n_diskname == MAX_DNAMES)
        {
          fprintf (stderr, _("Exceeded maximum disk count %d"),
              arguments->n_diskname);
          fprintf (stderr, "\n");
          argp_usage (state);
          break;
        }
      arguments->diskname[arguments->n_diskname++] = grub_strdup (arg);
      break;
    case 'e':
      grub_bhyve_unset_vgainsert ();
      break;
    case 'H':
      arguments->hold = (arg ? atoi (arg) : -1);
      break;
    case 'M':
      if (grub_bhyve_parse_memsize (arg, &arguments->memsz) != 0)
        {
          fprintf (stderr, _("Invalid guest memory size `%s'."), arg);
          fprintf (stderr, "\n");
          argp_usage (state);
        }
      break;
    case 'n':
      grub_bhyve_unset_cinsert ();
      break;
    case 'v':
      arguments->vmname = grub_strdup (arg);
      break;
    case 'S':
      grub_bhyve_set_memwire();
      break;

    case ARGP_KEY_ARG:
      {
        /* Too many arguments. */
        fprintf (stderr, _("Unknown extra argument `%s'."), arg);
        fprintf (stderr, "\n");
        argp_usage (state);
      }
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {
  options, argp_parser, NULL,
  N_("GRUB bhyve loader."),
  NULL, help_filter, NULL
};

#pragma clang diagnostic ignored "-Wmissing-prototypes"

int
main (int argc, char *argv[])
{
  struct arguments arguments =
    { 
      .memsz = DEFAULT_GUESTMEM * MB,
      .n_diskname = 0,
      .hold = 0,
    };
  size_t total_module_size = sizeof (struct grub_module_info);
  struct grub_module_info *modinfo;
  void *mods;
  volatile int hold = 0;

  set_program_name (argv[0]);

  grub_util_host_init (&argc, &argv);

  if (argp_parse (&argp, argc, argv, 0, 0, &arguments) != 0)
    {
      fprintf (stderr, "%s", _("Error in parsing command line arguments\n"));
      exit (1);
    }

  if (arguments.vmname == NULL)
    {
      fprintf (stderr, "%s", _("Required VM name parameter not supplied\n"));
      argp_help (&argp, stderr, ARGP_HELP_STD_USAGE, (char *)program_name);
      exit (1);
    }

  if (grub_bhyve_init (arguments.vmname, arguments.memsz) != 0)
    {
      fprintf (stderr, "%s", _("Error initializing VM\n"));
      exit (1);
    }

  mods = grub_malloc (total_module_size);
  modinfo = grub_memset (mods, 0, total_module_size);
  mods = (char *) (modinfo + 1);

  modinfo->magic = GRUB_MODULE_MAGIC;
  modinfo->offset = sizeof (struct grub_module_info);
  modinfo->size = total_module_size;

  grub_modbase = (grub_addr_t) modinfo;

  hold = arguments.hold;
  if (hold)
    printf (_("Run `gdb %s %d', and set ARGS.HOLD to zero.\n"),
            program_name, (int) getpid ());

  while (hold)
    {
      if (hold > 0)
        hold--;

      sleep (1);
    }

  signal (SIGINT, SIG_IGN);
  grub_console_init ();

  grub_init_all ();
  grub_bhyve_diskimg_init();

  if (arguments.n_diskname)
    {
      int d;

      for (d = 0; d < arguments.n_diskname; d++)
        grub_diskimg_add(arguments.diskname[d]);
    }

  /* Start GRUB!  */
  if (setjmp (main_env) == 0)
    grub_main ();

  grub_bhyve_diskimg_fini();
  grub_fini_all ();

  grub_machine_fini (GRUB_LOADER_FLAG_NORETURN);

  grub_free (mods);

  return 0;
}

/* vim: se tw=79 ts=2 sw=2 et cinoptions={1s: */
